int x,y; 
int raggio=15; 

void setup ()
{
  size(300,300); 
  background(0); 
  fill(180,50,21); 
  ellipseMode(RADIUS); 
  x=width/2; 
  y=height/2; 
}

void draw () 
{
  background(0);
  ellipse(x,y,20,20); 
  if (keyPressed)
  {
    if(key==CODED)
    {
      if (keyCode==UP) y-=5;     //keyCode è necessario con i caratteri speciali, come le frecce
      if (keyCode==DOWN) y+=5;
      if (keyCode==LEFT) x-=5;
      if (keyCode==RIGHT) x+=5;
    }
    x=constrain(x,20,width-20);    //non fa uscire dai bordi il pallino
    y=constrain(y,20,height-20);   //idem
  }
}

