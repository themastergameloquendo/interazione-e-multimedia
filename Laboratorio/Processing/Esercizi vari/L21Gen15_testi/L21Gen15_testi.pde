String [] ingresso; 
int righe; 

void setup () 
{
  ingresso=loadStrings("mamma.txt"); 
  righe=ingresso.length; 
  size(400,righe*32); 
  stroke(255);
  background(0); 
  textSize(20);   
}

void draw() 
{
  textAlign(RIGHT); 
  for(int i=0; i<righe; i++)
  {
    String [] temp=split(ingresso[i], ' '); 
    for (int j=0; j<temp.length; j++)
        text(ingresso[i], width-120, 30+30*i); 
  }
  textAlign(LEFT); 
  for(int i=0; i<righe; i++)
  {
    String [] temp=split(ingresso[i], ' '); 
    for (int j=0; j<temp.length; j++)
        text(ingresso[i], width-120, 30+30*i); 
  }
}
