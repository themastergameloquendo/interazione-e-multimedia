int X=640;
int Y=480;
int posX;
int posY;
float R=0;
float Tx=0;
float Ty=0;
float Sx=1;
float Sy=1;
void settings()
{
  size(X,Y);
}

void setup()
{
  background(255);
  rectMode(CENTER);
  
  stroke(0,0,255);
  strokeWeight(5);
  fill(255,255,0);
  
  posX=width/2;
  posY=height/2;

}

void draw()
{
  background(255);
  
  R=R+0.02;
  Tx=Tx+1;
  Ty=Ty+1;
  Sx=Sx+0.005;
  Sy=Sy+0.005;
  
  stroke(0,0,255);
  
  pushMatrix();
  translate(Tx,Ty);
  rect(0, 0, 50, 50);
  popMatrix();
  
  pushMatrix();
  translate(posX,posY);
  scale(Sx,Sy);
  rect(0, 0, 50, 50);
  popMatrix();
  
  pushMatrix();
  translate(posX,posY);
  rotate(R);
  rect(0, 0, 50, 50);
  popMatrix();
  
  
  pushMatrix();
  translate(posX-100,posY);
  shearX(R);
  rect(0, 0, 50, 50);
  popMatrix();
  
  
  stroke(255,0,0);
  pushMatrix();
  translate(posX+100,posY);
  applyMatrix(cos(R), -sin(R),0,
              sin(R), cos(R),0);
  rect(0, 0, 50, 50);
  popMatrix();
 
}