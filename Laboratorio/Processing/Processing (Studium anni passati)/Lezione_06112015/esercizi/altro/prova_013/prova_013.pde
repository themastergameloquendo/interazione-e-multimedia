import processing.pdf.*;

final int X=640;
final int Y=480;
String s="Interazione e Multimedia!";
int n=25;

float startEX;
float endEX;
float posY;
float minDim;
float maxDim;
float minColor;
float maxColor;

float newX;
float newColor;
float newDim;
  
void settings()
{
  size(X,Y,PDF, "Prova.pdf");
}

void setup()
{
  background(0);
  noSmooth();
  noStroke();
  int n=10;
  
  startEX=50;
  endEX=width-50;
  posY=300;
  minDim=10;
  maxDim=100;
  minColor=0;
  maxColor=255;
}

void draw()
{
  println("Inizio!");
 
  textSize(20); 
  text(s,50,50);
  
  
  for(int i=0;i<n;i++)
  {
    newColor=lerp(minColor,maxColor,((float)i)/n);
    newX=lerp(startEX,endEX,((float)i)/n);
    newDim=lerp(minDim,maxDim,((float)i)/n);
    
    fill(newColor);
    ellipse(newX,posY,newDim,newDim);
  }

  println("Fine!");
  exit();
}