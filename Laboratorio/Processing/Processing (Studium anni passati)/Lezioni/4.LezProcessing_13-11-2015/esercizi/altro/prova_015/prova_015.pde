Ball[] balls;
int N=30;

void setup()
{
  //background(0);
  size(500,500);
  balls=new Ball[N];
  
  for (int i=0; i<N;i++)
  {
    balls[i]=new Ball(random(60,width-60),random(60,height-60), random(1,5),random(1,5));
  }

}

void draw()
{
  fill(0,50);
  rect(0,0,width,height);
  for(int i=0;i<N;i++)
  {
    balls[i].run();
  }
}