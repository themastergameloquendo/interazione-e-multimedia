ArrayList<Ball> balls;
int N;

void setup()
{
  size(600,600);
  N=50;
  
  balls=new ArrayList<Ball>();
  
  for (int i=0;i<N-1;i++)
  {
    balls.add(new Ball(random(50,width-50),random(50,height-50),random(1,5),random(1,5)));
    //balls[i]=new Ball(random(50,width-50),random(50,height-50),random(1,5),random(1,5));
  }
  
  balls.add(new GreenBall(random(50,width-50),random(50,height-50),random(1,5),random(1,5)));
  
}

void draw()
{
  noStroke();
  fill(0,10);
  rect(0,0,width,height);
  
  //background(0);
  
  //for(int i=0;i<N;i++)
  //{
  //  balls.get(i).run();
  //}
  
  for (Ball b:balls)
  {
    b.run();
  }
 
}