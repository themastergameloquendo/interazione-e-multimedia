//Quantizzazione

int Q;
PImage image;
PImage qim;

void setup() 
{
  size(512, 512);
  image = loadImage("lena.png");
  image.filter(GRAY);
  
  Q=8;
  qim=quantizza(image,Q);
}

void draw() 
{
  image(qim,0,0);
  textSize(15);
  fill(255,0,0);
  text("Numero livelli: "+ Q,10,20);
}


PImage quantizza(PImage I, int k)
{
  PImage qI=I.copy();
  qI.loadPixels();
  
  for(int i=0;i<qI.pixels.length;i++)
  {
    float c=red(qI.pixels[i]);
    int qc= floor(k*(c/256));
    
    //Riporta i livelli in 0 e 255 per la visualizzazione 
    //(Normalizzazione)
    int nqc= floor((float(qc)/(k-1))*255);
    //int nqc= floor(lerp(0,255,float(qc)/(k-1)));
    
    qI.pixels[i]=color(nqc);
  }
  
  return qI;
}


void keyPressed()
{
  if (key=='+')
  {
    if (Q<256)
    {
      Q++;
      qim=quantizza(image,Q);
    }
  }
  
  if (key=='-')
  {
    if (Q>2)
    {
      Q--;
      qim=quantizza(image,Q);
    }
  }
  
  if (key=='o')
  { 
    Q=256;
    qim=image.copy();
  }
  
  if (key=='t')
  { 
    Q=2;
    qim=qim=quantizza(image,Q);
  }
}