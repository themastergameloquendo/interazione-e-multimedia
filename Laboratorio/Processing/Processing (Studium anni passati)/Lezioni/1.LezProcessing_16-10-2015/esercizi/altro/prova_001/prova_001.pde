// Setup; Draw; frameRate; variabili globali; println

final int X=300;
final int Y=300;
boolean white=true;

void setup()
{
  size(X,Y);
  background(0);
  frameRate(1);
  println("Inizio!");
}


void draw()
{
  if (white)
  {
    background(0);
    white=!white;
    println("Nero!");
  }
  else
  {  
    background(255);
    white=!white;
    println("Bianco!");
  }
    
}
