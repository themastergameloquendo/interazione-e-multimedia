PImage image;
PImage imG;

float[] HO;


void setup()
{
  size(512,512);
  image=loadImage("lena.png");
  
  image.resize(image.width/2,image.height/2);
  imG=gamma(image,5);
  
  image(image,0,0);
  image(imG,0,image.height);
  
  
  
}

PImage gamma(PImage I,float g)
{
  PImage E= I.copy();
  
  
  float MaxR=red(E.pixels[0]);
  float MaxG=green(E.pixels[0]);
  float MaxB=blue(E.pixels[0]);
  
  for(int i=0;i<E.pixels.length; i++)
  {
    if((red(E.pixels[i]))>MaxR)
    {
      MaxR=(red(E.pixels[i]));
    }
    
    if((green(E.pixels[i]))>MaxG)
    {
      MaxG=(green(E.pixels[i]));
    }
    
    if((blue(E.pixels[i]))>MaxB)
    {
      MaxB=(blue(E.pixels[i]));
    }    
  }
  
  float cr=255*(1/pow(MaxR,g));
  float cg=255*(1/pow(MaxG,g));
  float cb=255*(1/pow(MaxB,g));
  
  int nr;
  int ng;
  int nb;
  
  for(int i=0;i<E.pixels.length;i++)
  {
    nr=int(cr*(pow(red(E.pixels[i]),g)));
    ng=int(cg*(pow(green(E.pixels[i]),g)));
    nb=int(cb*(pow(blue(E.pixels[i]),g)));
    
    E.pixels[i]=color(nr,ng,nb);
  }
  
  E.updatePixels();
  return E;
}