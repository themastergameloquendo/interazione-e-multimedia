//Operatori locali - In questo caso operatori di rango (NON sono lineari -> NON si può usare la convoluzione)
// Mediano, e Min

PImage image;
PImage imMin;
PImage imMediano;

void setup()
{
  size(512, 512);
  image=loadImage("lena.png");
  image.resize(image.width/2,image.height/2);
  image.filter(GRAY);
  
  image(image,0,0);
  
  imMin=opMin(image,25);
  
  image(imMin,image.width,0);
  
  imMediano=opMediano(image,7);
  image(imMediano,image.width,image.height);
  

}


PImage opMin(PImage I,int N)
{
  PImage R=createImage(I.width,I.height,RGB);
  PImage tmp;
  int off=N/2;
  
  for(int i=0;i<I.width;i++)
  {
    for(int j=0;j<I.height;j++)
    {
      tmp=I.get(max(0,i-off),max(0,j-off),min(N,I.width-i),min(N,I.height-j));
      
      tmp.loadPixels();
       float vMin=green(tmp.pixels[0]);
       for(int k=0; k<tmp.pixels.length; k++)
       {
         if (green(tmp.pixels[k])<vMin)
             vMin=green(tmp.pixels[k]);
       }
       
       R.set(i,j,color(vMin));
    }
  }
  return R;
  
}

PImage opMediano(PImage I,int N)
{
  PImage R=createImage(I.width,I.height,RGB);
  PImage tmp;
  int off=N/2;
  color[] colors;
  
  for(int i=0;i<I.width;i++)
  {
    for(int j=0;j<I.height;j++)
    {
      tmp=I.get(max(0,i-off),max(0,j-off),min(N,I.width-i),min(N,I.height-j));
      
      tmp.loadPixels();
      //
      colors=sort(tmp.pixels);
      
      int pos=colors.length/2;
      color mediano;
      
      if(colors.length%2==1)
      {
        mediano=colors[pos];
      }
      else
      {
        //mediano=((colors[pos])+(colors[pos-1]))/2;
        //SBAGLIATO! Non si può fare la media di due color direttamente
        //Prima bisogna ottenere i valori float dei canali interessati (per scala di grigi uno qualunque)
        mediano=color(green(colors[pos])+green(colors[pos-1]))/2;
      }
      
      R.set(i,j,mediano);
    }
  }
  return R;
  
}