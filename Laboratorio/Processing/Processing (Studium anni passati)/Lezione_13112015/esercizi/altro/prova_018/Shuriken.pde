class Shuriken
{
  float posX;
  float posY;
  float speedX;
  float speedY;
  
  float R=0;
  boolean lanciato=false;
  
  Shuriken(float X, float Y, float sX, float sY)
  {
    posX=X;
    posY=Y;
    speedX=sX;
    speedY=sY;
  }
  
  void display()
  { 

    pushMatrix();
    translate(posX,posY);
    rotate(R);
    
    //Inizio disegno shuriken
    rectMode(CENTER);
    noStroke();
    fill(128);
    rect(0,0,30,30);
    
    pushMatrix();
    rotate(radians(45));
    rect(0,0,30,30);
    popMatrix();
    
    noStroke();
    fill(255);
    ellipse(0,0,7,7);
    //Fine disegno shuriken
    
    popMatrix();
  }
  
  void controllaLancio()
  {
    if ((mousePressed)&&(dist(posX,posY,mouseX,mouseY)<30))
      lanciato=true;
  }
  
  void muovi()
  {
    R=R+radians(10);
    posX=posX+speedX;
    posY=posY+speedY;
  }
  
  void run()
  {
    controllaLancio();
    
    if (lanciato)
    {
      muovi();
    }
    
    display();
  }
  
}