Shuriken s1;
Shuriken s2;

void setup()
{
  size(500,500);
  s1=new Shuriken(30,50,5,0);
  s2=new Shuriken(30,150,10,0);
}

void draw()
{
  background(255);
  s1.run();
  s2.run();
}