Nella lezione di oggi sono stati svolti due esercizi che riguardano la parte del programma svolta finora. I due esercizi sono tratti da compiti d'esame svolti negli anni precedenti.

I testi degli esercizi si trovano le file PDF.

Nella cartella "aula" sono presenti i sorgenti per risolvere gli esercizi scritti durante la lezione. Il secondo esercizio manca degli ultimi tre punti, tuttavia semplici da svolgere e presenti nella soluzione pronta nella cartella "altro".

Attenzione, ci sono differenze tra la risoluzione vista in aula e quelle gi� svolte. Questo fa capire la possibilit� di scegliere diversi approcci.