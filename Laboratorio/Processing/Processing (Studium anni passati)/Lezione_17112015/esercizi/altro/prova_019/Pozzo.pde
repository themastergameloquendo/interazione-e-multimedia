class Pozzo
{
 public float posX;
 public float posY;
  
 Pozzo(float posX,float posY)
 {
   this.posX=posX;
   this.posY=posY;
 }
  
 void display()
 {
   noStroke();
   fill(0,128,255);
    
   ellipse(posX,posY,30,30);
 }
}