ArrayList<Bullet> bullets= new ArrayList<Bullet>();
ArrayList<Warp> warps=new ArrayList<Warp>();
int K=10;

void setup()
{
  size(600,600);
  background(255);
  
  for (int i=0;i<K; i++)
  {
    warps.add(new Warp(random(50,width-50),random(50,height-50),random(0,360),constrain(random(1,11),1,10),color(floor(random(0,256)),floor(random(0,256)),floor(random(0,256)))));
  }

}

void draw()
{
  background(255);
  
  for (Warp w:warps)
  {
    w.run();
  }
  
  for (Bullet b:bullets)
  {
    b.run();
  }
  
}

void mousePressed()
{
  for (Warp w:warps)
  {
    if (w.checkMouse()) bullets.add(w.shot());
  }
}