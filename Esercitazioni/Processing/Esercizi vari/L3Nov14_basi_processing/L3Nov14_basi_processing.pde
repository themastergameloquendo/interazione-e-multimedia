//per trovare i giusti parametri, basta evidenziare la parola "size" e cliccare su "Find in Reference" 

size(400, 400); 
background(0, 255, 0); //indica il colore di sfondo
stroke (255); //attribuisce il colore alla retta -> stroke=tratto. NB. VA PRIMA DEL COMANDO "line"!
strokeWeight(5); //definisce lo spessore della linea in pixel
line(150, 25, 270, 350); //disegna un segmento; le prime due coordinate indicano il primo punto, le ultime due il secondo. A(150, 25) - B (270, 350)
//i disegni successivi SOVRASCRIVONO quelli precedenti, senza lasciare traccia sul piano di lavoro, per così dire. 


background(0); //può andare da 0 a 255 (scala di grigi) 
background(255,0,0); //in RGB

stroke(#3B5DB7); //permette di stabilire i colori secondo il sistema web. Andando in Tools->Color Selector, è possibile definire il colore da qui.
//per stabilire la trasparenza (canale alfa), adoperiamo: 
stroke(128,128,128,0); //il quarto valore è il canale alfa - va da 0 a 255
line(0,0,400,400);

//IL main () E' PROIBITOO!! TESSSSSOOORRRRRO. Vada retro main! in quanto il "main" è già implicitamente presente nel programma, anche se vuoto.

int x=100, y=100; //dichiarazione variabili
int doppio(int x) {return 2*x;} //raddoppia le dimensioni
void setup () {size(doppio(x), doppio(doppio(y)));}

//lo schema per un disegno ordinato è: 
//dichiarazione di eventuali variabili globali 
//sovrascrivo i metodi setup di default
void setup () {
size(200,200); 
background(x+y); 
}
//metodo di aggiornamento schermo eseguito in ciclo infinito
void draw () {
  x=x+1; 
  println(x); //stampa in console il valore di x (può stampare anche stringhe predefinite - ex. String s="The size is"; ) 
  background(x); 
}
